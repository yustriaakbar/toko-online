<?php
session_start();
//koneksi ke database
$koneksi = new mysqli("localhost","root","","toko_online");
?>
<!DOCTYPE html>
<html>
<head>
	<title>Toko Online</title>
	<link rel="stylesheet" type="text/css" href="admin/assets/css/bootstrap.css">
</head>
<body>

 <nav class="navbar navbar-default">
	<div class="container">
		<ul class="nav navbar-nav">
			<li><a href="index.php">Home</a></li>
			<li><a href="keranjang.php">Keranjang</a></li>
			<!-- jika sudah login(ada session pelanggan) -->
			<?php if (isset($_SESSION["pelanggan"])): ?>
				<li><a href="logout.php">Logout</a></li>
			<!-- selain itu (belum login=belum ada session pelaggan) -->
			<?php else: ?>
			<li><a href="login.php">Login</a></li>
			<li><a href="daftar.php">Daftar</a></li>
			<?php endif ?>
			<li><a href="checkout.php">Checkout</a></li>	
		</ul>
	</div>
</nav>
<section class="konten">
	<div class="container" align="middle">
		<img src="gambar3.jpg" width="300" height="300">
		<img src="gambar1.jpg" width="300" height="300">
		<img src="gambar2.jpg" width="300" height="300">
	</div>
</section>


<section class="konten">
	<div class="container">
		<br>
		
		<div class="row">

<!-- Ini Produk -->
			<?php $ambil = $koneksi->query("SELECT * FROM produk"); ?>
			<?php while($perproduk = $ambil->fetch_assoc()){ ?>
			<div class="col-md-3">
				<div class=thumbnail>
					<img src="foto_produk/<?php echo $perproduk['foto_produk']; ?>" alt="">
					<div class="caption">
						<h4><?php echo $perproduk['nama_produk']; ?></h4>
						<h5>Rp. <?php echo $perproduk['harga_produk']; ?></h5>
						<a href="beli.php?id=<?php echo $perproduk['id_produk'];?>" class="btn btn-primary">Beli</a>
						<a href="detail.php?id=<?php echo $perproduk["id_produk"]; ?>" class="btn btn-default">Detail</a>
					</div>
				</div>
			</div>
			<?php } ?>

		</div>
	</div>
</section>

</body>
</html>