-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 15, 2020 at 05:03 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `toko_online`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `username`, `password`, `nama_lengkap`) VALUES
(0, 'yustriaakbar', 'Donnarumma99', 'Yustria Akbar');

-- --------------------------------------------------------

--
-- Table structure for table `pelanggan`
--

CREATE TABLE `pelanggan` (
  `id_pelanggan` int(11) NOT NULL,
  `email_pelanggan` varchar(50) NOT NULL,
  `password_pelanggan` varchar(100) NOT NULL,
  `nama_pelanggan` varchar(100) NOT NULL,
  `telepon_pelanggan` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pelanggan`
--

INSERT INTO `pelanggan` (`id_pelanggan`, `email_pelanggan`, `password_pelanggan`, `nama_pelanggan`, `telepon_pelanggan`) VALUES
(1, 'namasayagondrong@gmail.com', 'sayamasihgondrong', 'Gondrong Indonesia', '082232427594');

-- --------------------------------------------------------

--
-- Table structure for table `pembelian`
--

CREATE TABLE `pembelian` (
  `id_pembelian` int(11) NOT NULL,
  `id_pelanggan` int(11) NOT NULL,
  `tanggal_pembelian` date NOT NULL,
  `total_pembelian` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembelian`
--

INSERT INTO `pembelian` (`id_pembelian`, `id_pelanggan`, `tanggal_pembelian`, `total_pembelian`) VALUES
(1, 1, '2019-09-05', 4000000),
(2, 1, '2019-09-04', 5000000);

-- --------------------------------------------------------

--
-- Table structure for table `pembelian_produk`
--

CREATE TABLE `pembelian_produk` (
  `id_pembelian_produk` int(11) NOT NULL,
  `id_pembelian` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembelian_produk`
--

INSERT INTO `pembelian_produk` (`id_pembelian_produk`, `id_pembelian`, `id_produk`) VALUES
(1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `id_produk` int(11) NOT NULL,
  `nama_produk` varchar(100) NOT NULL,
  `harga_produk` int(11) NOT NULL,
  `berat` int(11) NOT NULL,
  `foto_produk` varchar(100) NOT NULL,
  `deskripsi_produk` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id_produk`, `nama_produk`, `harga_produk`, `berat`, `foto_produk`, `deskripsi_produk`) VALUES
(1, 'Alpina Jacket Wood Brown Canvas Solid', 289000, 250, 'jacket1.jpg', 'Tersedia size: S M L XL XXL XXXL<br>\r\nMaterial bahan: Canvas<br>\r\nSize Chart Jacket<br>\r\nS = Width 46, Height 66, Sleeve 62, Sleeve Width 15, Shoulder Width 41<br>\r\nM = Width 50, Height 67, Sleeve 62, Sleeve Width 16, Shoulder Width 43<br>\r\nL = Width 52, Height 67, Sleeve 62, Sleeve Width 17, Shoulder Width 47<br>\r\nXL = Width 54, Height 67, Sleeve 62, Sleeve Width 17,5, Shoulder Width 49<br>\r\nXXL = Width 60, Height 67, Sleeve 62, Sleeve Width 18, Shoulder Width 52<br>\r\nXXXL = Width 66, Height 67, Sleeve 62, Sleeve Width 19,5, Shoulder Width 55<br>\r\nKetentuan Return :<br>\r\nBisa, jika stock masih tersedia. Berlaku tukar size saja, tidak berlaku tukar artikel. Maksimal H+1 setelah barang diterima. Ongkir bolak-balik ditanggung pembeli.'),
(2, 'Adams Jacket Navy Canvas Solid', 282000, 250, 'jacket5.jpeg', 'Tersedia size: S M L XL XXL XXXL<br>\r\nMaterial bahan: Canvas<br>\r\nSize Chart Jacket<br>\r\nS = Width 46, Height 66, Sleeve 62, Sleeve Width 15, Shoulder Width 41<br>\r\nM = Width 50, Height 67, Sleeve 62, Sleeve Width 16, Shoulder Width 43<br>\r\nL = Width 52, Height 67, Sleeve 62, Sleeve Width 17, Shoulder Width 47<br>\r\nXL = Width 54, Height 67, Sleeve 62, Sleeve Width 17,5, Shoulder Width 49<br>\r\nXXL = Width 60, Height 67, Sleeve 62, Sleeve Width 18, Shoulder Width 52<br>\r\nXXXL = Width 66, Height 67, Sleeve 62, Sleeve Width 19,5, Shoulder Width 55<br>\r\nKetentuan Return :<br>\r\nBisa, jika stock masih tersedia. Berlaku tukar size saja, tidak berlaku tukar artikel. Maksimal H+1 setelah barang diterima. Ongkir bolak-balik ditanggung pembeli.'),
(3, 'Adams Jacket Dark Brown Canvas Solid', 279000, 250, 'jacket11.jpg', 'Tersedia size: S M L XL XXL XXXL<br>\r\nMaterial bahan: Canvas<br>\r\nSize Chart Jacket<br>\r\nS = Width 46, Height 66, Sleeve 62, Sleeve Width 15, Shoulder Width 41<br>\r\nM = Width 50, Height 67, Sleeve 62, Sleeve Width 16, Shoulder Width 43<br>\r\nL = Width 52, Height 67, Sleeve 62, Sleeve Width 17, Shoulder Width 47<br>\r\nXL = Width 54, Height 67, Sleeve 62, Sleeve Width 17,5, Shoulder Width 49<br>\r\nXXL = Width 60, Height 67, Sleeve 62, Sleeve Width 18, Shoulder Width 52<br>\r\nXXXL = Width 66, Height 67, Sleeve 62, Sleeve Width 19,5, Shoulder Width 55<br>\r\nKetentuan Return :<br>\r\nBisa, jika stock masih tersedia. Berlaku tukar size saja, tidak berlaku tukar artikel. Maksimal H+1 setelah barang diterima. Ongkir bolak-balik ditanggung pembeli.'),
(4, 'Chester Jacket Black Canvas Solid', 285000, 250, 'jacket12.jpg', 'Tersedia size: S M L XL XXL XXXL<br>\r\nMaterial bahan: Canvas<br>\r\nSize Chart Jacket<br>\r\nS = Width 46, Height 66, Sleeve 62, Sleeve Width 15, Shoulder Width 41<br>\r\nM = Width 50, Height 67, Sleeve 62, Sleeve Width 16, Shoulder Width 43<br>\r\nL = Width 52, Height 67, Sleeve 62, Sleeve Width 17, Shoulder Width 47<br>\r\nXL = Width 54, Height 67, Sleeve 62, Sleeve Width 17,5, Shoulder Width 49<br>\r\nXXL = Width 60, Height 67, Sleeve 62, Sleeve Width 18, Shoulder Width 52<br>\r\nXXXL = Width 66, Height 67, Sleeve 62, Sleeve Width 19,5, Shoulder Width 55<br>\r\nKetentuan Return :<br>\r\nBisa, jika stock masih tersedia. Berlaku tukar size saja, tidak berlaku tukar artikel. Maksimal H+1 setelah barang diterima. Ongkir bolak-balik ditanggung pembeli.'),
(5, 'Adams Jacket Green', 283000, 250, 'jacket2.jpeg', 'Tersedia size: S M L XL XXL XXXL<br>\r\nMaterial bahan: Canvas<br>\r\nSize Chart Jacket<br>\r\nS = Width 46, Height 66, Sleeve 62, Sleeve Width 15, Shoulder Width 41<br>\r\nM = Width 50, Height 67, Sleeve 62, Sleeve Width 16, Shoulder Width 43<br>\r\nL = Width 52, Height 67, Sleeve 62, Sleeve Width 17, Shoulder Width 47<br>\r\nXL = Width 54, Height 67, Sleeve 62, Sleeve Width 17,5, Shoulder Width 49<br>\r\nXXL = Width 60, Height 67, Sleeve 62, Sleeve Width 18, Shoulder Width 52<br>\r\nXXXL = Width 66, Height 67, Sleeve 62, Sleeve Width 19,5, Shoulder Width 55<br>\r\nKetentuan Return :<br>\r\nBisa, jika stock masih tersedia. Berlaku tukar size saja, tidak berlaku tukar artikel. Maksimal H+1 setelah barang diterima. Ongkir bolak-balik ditanggung pembeli.'),
(6, 'Adams Jacket Black', 283000, 1000, 'jacket6.jpeg', 'Tersedia size: S M L XL XXL XXXL<br>\r\nMaterial bahan: Canvas<br>\r\nSize Chart Jacket<br>\r\nS = Width 46, Height 66, Sleeve 62, Sleeve Width 15, Shoulder Width 41<br>\r\nM = Width 50, Height 67, Sleeve 62, Sleeve Width 16, Shoulder Width 43<br>\r\nL = Width 52, Height 67, Sleeve 62, Sleeve Width 17, Shoulder Width 47<br>\r\nXL = Width 54, Height 67, Sleeve 62, Sleeve Width 17,5, Shoulder Width 49<br>\r\nXXL = Width 60, Height 67, Sleeve 62, Sleeve Width 18, Shoulder Width 52<br>\r\nXXXL = Width 66, Height 67, Sleeve 62, Sleeve Width 19,5, Shoulder Width 55<br>\r\nKetentuan Return :<br>\r\nBisa, jika stock masih tersedia. Berlaku tukar size saja, tidak berlaku tukar artikel. Maksimal H+1 setelah barang diterima. Ongkir bolak-balik ditanggung pembeli.'),
(7, 'Exventura Jeans White', 329000, 250, 'jacket7.jpg', 'Tersedia size: S M L XL XXL XXXL<br>\r\nMaterial bahan: Canvas<br>\r\nSize Chart Jacket<br>\r\nS = Width 46, Height 66, Sleeve 62, Sleeve Width 15, Shoulder Width 41<br>\r\nM = Width 50, Height 67, Sleeve 62, Sleeve Width 16, Shoulder Width 43<br>\r\nL = Width 52, Height 67, Sleeve 62, Sleeve Width 17, Shoulder Width 47<br>\r\nXL = Width 54, Height 67, Sleeve 62, Sleeve Width 17,5, Shoulder Width 49<br>\r\nXXL = Width 60, Height 67, Sleeve 62, Sleeve Width 18, Shoulder Width 52<br>\r\nXXXL = Width 66, Height 67, Sleeve 62, Sleeve Width 19,5, Shoulder Width 55<br>\r\nKetentuan Return :<br>\r\nBisa, jika stock masih tersedia. Berlaku tukar size saja, tidak berlaku tukar artikel. Maksimal H+1 setelah barang diterima. Ongkir bolak-balik ditanggung pembeli.'),
(8, 'Exventura Jeans Oreo Black', 329000, 250, 'jacket3.jpeg', 'Tersedia size: S M L XL XXL XXXL<br>\r\nMaterial bahan: Canvas<br>\r\nSize Chart Jacket<br>\r\nS = Width 46, Height 66, Sleeve 62, Sleeve Width 15, Shoulder Width 41<br>\r\nM = Width 50, Height 67, Sleeve 62, Sleeve Width 16, Shoulder Width 43<br>\r\nL = Width 52, Height 67, Sleeve 62, Sleeve Width 17, Shoulder Width 47<br>\r\nXL = Width 54, Height 67, Sleeve 62, Sleeve Width 17,5, Shoulder Width 49<br>\r\nXXL = Width 60, Height 67, Sleeve 62, Sleeve Width 18, Shoulder Width 52<br>\r\nXXXL = Width 66, Height 67, Sleeve 62, Sleeve Width 19,5, Shoulder Width 55<br>\r\nKetentuan Return :<br>\r\nBisa, jika stock masih tersedia. Berlaku tukar size saja, tidak berlaku tukar artikel. Maksimal H+1 setelah barang diterima. Ongkir bolak-balik ditanggung pembeli.'),
(9, 'Exventura Jeans Oreo Blue', 329000, 250, 'jacket9.jpg', 'Tersedia size: S M L XL XXL XXXL<br>\r\nMaterial bahan: Canvas<br>\r\nSize Chart Jacket<br>\r\nS = Width 46, Height 66, Sleeve 62, Sleeve Width 15, Shoulder Width 41<br>\r\nM = Width 50, Height 67, Sleeve 62, Sleeve Width 16, Shoulder Width 43<br>\r\nL = Width 52, Height 67, Sleeve 62, Sleeve Width 17, Shoulder Width 47<br>\r\nXL = Width 54, Height 67, Sleeve 62, Sleeve Width 17,5, Shoulder Width 49<br>\r\nXXL = Width 60, Height 67, Sleeve 62, Sleeve Width 18, Shoulder Width 52<br>\r\nXXXL = Width 66, Height 67, Sleeve 62, Sleeve Width 19,5, Shoulder Width 55<br>\r\nKetentuan Return :<br>\r\nBisa, jika stock masih tersedia. Berlaku tukar size saja, tidak berlaku tukar artikel. Maksimal H+1 setelah barang diterima. Ongkir bolak-balik ditanggung pembeli.'),
(10, 'Adams Jacket Green Phanter', 279000, 250, 'jacket10.jpg', 'Tersedia size: S M L XL XXL XXXL<br>\r\nMaterial bahan: Canvas<br>\r\nSize Chart Jacket<br>\r\nS = Width 46, Height 66, Sleeve 62, Sleeve Width 15, Shoulder Width 41<br>\r\nM = Width 50, Height 67, Sleeve 62, Sleeve Width 16, Shoulder Width 43<br>\r\nL = Width 52, Height 67, Sleeve 62, Sleeve Width 17, Shoulder Width 47<br>\r\nXL = Width 54, Height 67, Sleeve 62, Sleeve Width 17,5, Shoulder Width 49<br>\r\nXXL = Width 60, Height 67, Sleeve 62, Sleeve Width 18, Shoulder Width 52<br>\r\nXXXL = Width 66, Height 67, Sleeve 62, Sleeve Width 19,5, Shoulder Width 55<br>\r\nKetentuan Return :<br>\r\nBisa, jika stock masih tersedia. Berlaku tukar size saja, tidak berlaku tukar artikel. Maksimal H+1 setelah barang diterima. Ongkir bolak-balik ditanggung pembeli.'),
(11, 'Adams Jacket Cream Canvas Solid', 269000, 250, 'jacket8.jpg', 'Tersedia size: S M L XL XXL XXXL<br>\r\nMaterial bahan: Canvas<br>\r\nSize Chart Jacket<br>\r\nS = Width 46, Height 66, Sleeve 62, Sleeve Width 15, Shoulder Width 41<br>\r\nM = Width 50, Height 67, Sleeve 62, Sleeve Width 16, Shoulder Width 43<br>\r\nL = Width 52, Height 67, Sleeve 62, Sleeve Width 17, Shoulder Width 47<br>\r\nXL = Width 54, Height 67, Sleeve 62, Sleeve Width 17,5, Shoulder Width 49<br>\r\nXXL = Width 60, Height 67, Sleeve 62, Sleeve Width 18, Shoulder Width 52<br>\r\nXXXL = Width 66, Height 67, Sleeve 62, Sleeve Width 19,5, Shoulder Width 55<br>\r\nKetentuan Return :<br>\r\nBisa, jika stock masih tersedia. Berlaku tukar size saja, tidak berlaku tukar artikel. Maksimal H+1 setelah barang diterima. Ongkir bolak-balik ditanggung pembeli.'),
(12, 'Adams Jacket Navy Extra Pocket', 279000, 250, 'jacket4.jpeg', 'Tersedia size: S M L XL XXL XXXL<br>\r\nMaterial bahan: Canvas<br>\r\nSize Chart Jacket<br>\r\nS = Width 46, Height 66, Sleeve 62, Sleeve Width 15, Shoulder Width 41<br>\r\nM = Width 50, Height 67, Sleeve 62, Sleeve Width 16, Shoulder Width 43<br>\r\nL = Width 52, Height 67, Sleeve 62, Sleeve Width 17, Shoulder Width 47<br>\r\nXL = Width 54, Height 67, Sleeve 62, Sleeve Width 17,5, Shoulder Width 49<br>\r\nXXL = Width 60, Height 67, Sleeve 62, Sleeve Width 18, Shoulder Width 52<br>\r\nXXXL = Width 66, Height 67, Sleeve 62, Sleeve Width 19,5, Shoulder Width 55<br>\r\nKetentuan Return :<br>\r\nBisa, jika stock masih tersedia. Berlaku tukar size saja, tidak berlaku tukar artikel. Maksimal H+1 setelah barang diterima. Ongkir bolak-balik ditanggung pembeli.');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `pelanggan`
--
ALTER TABLE `pelanggan`
  ADD PRIMARY KEY (`id_pelanggan`);

--
-- Indexes for table `pembelian`
--
ALTER TABLE `pembelian`
  ADD PRIMARY KEY (`id_pembelian`);

--
-- Indexes for table `pembelian_produk`
--
ALTER TABLE `pembelian_produk`
  ADD PRIMARY KEY (`id_pembelian_produk`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id_produk`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
